package com.avigilon.YAML_comments_cleaner;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class Primary extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton sourceButton;
    private JTextField sourceField;
    private JButton outputButton;
    private JTextField outputField;
    private JRadioButton radioButtonAll;
    private JRadioButton radioButtonWith;
    private JRadioButton radioButtonWithout;
    private JRadioButton radioButtonRegEx;
    private JTextField regExField;
    private JTextField wordField;
    private JProgressBar progressBar1;
    private File defaultLocation = new File(".");


    public Primary() {
        super(new DummyFrame("YAML Comment Cleaner"));
        setTitle("YAML Comment Cleaner");
        setUndecorated(false);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        ActionListener radioButtonAL = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (e.getActionCommand()) {
                    case "All comments":
                        wordField.setEditable(false);
                        regExField.setEditable(false);
                        break;
                    case "With Word":
                        wordField.setEditable(true);
                        regExField.setEditable(false);
                        break;
                    case "WithOut Word":
                        wordField.setEditable(true);
                        regExField.setEditable(false);
                        break;
                    case "Customize RegEx":
                        wordField.setEditable(false);
                        regExField.setEditable(true);

                }
            }
        };

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                progressBar1.setIndeterminate(true); //Make progress bar moving
                onOK();
                progressBar1.setIndeterminate(false); // Stop progress bar.
                JOptionPane.showMessageDialog(null, "Done");
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        sourceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseSource();
            }
        });

        outputButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseOutput();
            }
        });

        radioButtonAll.addActionListener(radioButtonAL);
        radioButtonWith.addActionListener(radioButtonAL);
        radioButtonWithout.addActionListener(radioButtonAL);
        radioButtonRegEx.addActionListener(radioButtonAL);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    private void onOK() {
        String regEx;
        if (radioButtonAll.isSelected()) {
            regEx = "(##.*)(.*##)";
        } else if (radioButtonWith.isSelected()) {
            regEx = "(##.*)(" + wordField.getText() + ")(.*##)";
        } else if (radioButtonWithout.isSelected()) {
            regEx = "(##)[\\s](?!" + wordField.getText() + ")(.*)(##)";
        } else {
            regEx = regExField.getText();
        }
        try {
            CmtCleaner cc = new CmtCleaner(sourceField.getText(), outputField.getText(), regEx);
            cc.runCleaner();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void chooseSource() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(defaultLocation);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            sourceField.setText(selectedFile.getAbsolutePath());
        }
    }

    private void chooseOutput() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(defaultLocation);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            outputField.setText(selectedFile.getAbsolutePath());
        }
    }

    public static void main(String[] args) {
        Primary dialog = new Primary();
        dialog.pack();
        dialog.setVisible(true);
        dialog.setAutoRequestFocus(true);
        System.exit(0);
    }

}

/**
 * A dummy frame to create a task bar entry.
 */
class DummyFrame extends JFrame {
    DummyFrame(String title) {
        super(title);
        setUndecorated(true);
        setVisible(true);
        setLocationRelativeTo(null);
    }
}
