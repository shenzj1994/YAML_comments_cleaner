package com.avigilon.YAML_comments_cleaner;


import java.io.*;
import java.util.ArrayList;
import java.util.List;


class CmtCleaner {
    private File srcFolder;
    private File destFolder;
    private String regEx;
    private List<File> listOfFiles = new ArrayList<>();


    /**
     * Construct a new CmtCleaner object.
     *
     * @param srcFolderPath  The path to source folder that contains the YAML file with comments to be cleaned.
     * @param destFolderPath The path to output folder that will used to store 'clean' YAML file.
     * @param regEx          A regular expression in a format of string that will be used to search the specific comments.
     * @throws IOException If any path above is invalid. The setter will throws IOException. Construct will pass the IOException to higher level.
     */
    CmtCleaner(String srcFolderPath, String destFolderPath, String regEx) throws IOException {
        setSrcFolder(srcFolderPath);
        setDestFolder(destFolderPath);
        setRegEx(regEx);
    }

    /**
     * Run the cleaner
     */
    void runCleaner() throws IOException {
        int fileCount = 0;
        List<File> srcFilesList = recursiveListing(srcFolder);

        for (File aSrcFile : srcFilesList) {
            //For all YAML files
            if (aSrcFile.getName().contains("yml")) {
                System.out.println("Processing: "+aSrcFile.getName());
                String outputFilePath;
                outputFilePath = destFolder.getAbsolutePath() + "\\" + srcFolder.toURI().relativize(aSrcFile.toURI()); //Get the path relative to source folder
                cleanYAML(aSrcFile, new File(outputFilePath));
                fileCount++;
            }
        }

        System.out.println("\nFound and Processed " + fileCount + " YAML file(s).");

    }

    /**
     * Set source folder.
     *
     * @param srcFolderPath The path to source folder that contains the YAML file with comments to be cleaned.
     *
     * @exception IOException If any path above is invalid. The setter will throws IOException.
     */
    private void setSrcFolder(String srcFolderPath) throws IOException {
        File f = new File(srcFolderPath);
        if (f.exists())
            this.srcFolder = f;
        else
            throw new IOException("File or directory not found");
    }

    /**
     * Set output folder.
     *
     * @param destFolderPath The path to output folder that will used to store 'clean' YAML file.
     *
     * @exception IOException If any path above is invalid. The setter will throws IOException.
     */
    private void setDestFolder(String destFolderPath) throws IOException {
        File f = new File(destFolderPath);
        if (!f.exists()&&!f.mkdirs()) //if folder does not assist and failed to create
            throw new IOException("Cannot access output folder.");
        else
            this.destFolder = f;
    }

    /**
     * Set the regular expression.
     *
     * @param regEx A regular expression in a format of string that will be used to search the specific comments.
     *
     * */

    private void setRegEx(String regEx) {
        this.regEx = regEx;
    }

    /**
     * Listing all files under a folder recursively
     *
     * @param folder The folder to list files.
     *
     * @return A list of files in a folder recursively.
     * */
    private List<File> recursiveListing(File folder) {
        for (File aFile : folder.listFiles()) {
            if (aFile.isFile()) {
                listOfFiles.add(aFile);
            } else {
                recursiveListing(aFile);
            }
        }

        return listOfFiles;
    }

    /**
     * Clean a single YAML file
     *
     * @param inputFile The source file.
     * @param  outputFile The output file after cleaning.
     *
     * @exception IOException Either input or output file that cannot be opened or written.
     * */
    private void cleanYAML(File inputFile, File outputFile) throws IOException {
        if (!outputFile.exists()) { //if file does not exist
            if (!outputFile.getParentFile().mkdirs() && !outputFile.createNewFile()) { //if file fails to create
                throw new IOException("Cannot create output file.");
            }
        }

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile.getAbsolutePath()),"UTF-8")); //Force UTF-8 encoding
        String line;
        //Go through the whole file
        while ((line = br.readLine()) != null) {
            line = cleanSingleLine(line, regEx);
            bw.write(line);
            bw.write("\n");
        }
        br.close();
        bw.close();

    }

    /**
     * Clean a single line
     *
     * @param line A line in String format.
     * @param  regEx The regular expression that used to find and delete the comments
     * */
    private String cleanSingleLine(String line, String regEx) {
        line = line.replaceAll(regEx, ""); //Delete the comment
        return line;
    }


}
