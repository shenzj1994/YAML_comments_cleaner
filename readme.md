# Summary
This is a little tool to clean up YAML file. It has the ability to do the following type of clean:
1. All comments
2. Comments with some keywords
3. Comments without some keywords
4. Customized RegEx

# System Requirement
You must have JRE to run the program. To build it, you need JDK as well.

# How to Build
Use the **com.avigilon.YAML_comments_cleaner.Primary** as main class.

# How to Use
The GUI itself is self-explanatory. Please leave comments if you have other questions.

# Current build status
[![pipeline status](https://gitlab.com/shenzj1994/YAML_comments_cleaner/badges/master/pipeline.svg)](https://gitlab.com/shenzj1994/YAML_comments_cleaner/commits/master)